package iwantstorage

import (
	"context"
	"errors"
	"io"
	"io/ioutil"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore"
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob"
)

func newAzBlobDriver(ctx context.Context, config *AzBlob) (driver *azBlobDriver, err error) {
	driver = &azBlobDriver{}
	// TODO: does azblob support context for initialization?
	var creds azcore.TokenCredential
	if config.ClientSecretCredential != nil {
		creds, err = azidentity.NewClientSecretCredential(
			config.ClientSecretCredential.TenantId,
			config.ClientSecretCredential.ClientId,
			config.ClientSecretCredential.ClientSecret,
			nil,
		)
	} else {
		return nil, &ErrBadConfig{errors.New("no credential defined, define ClientSecretCredential")}
	}
	if err != nil {
		return nil, err
	}
	driver.client, err = azblob.NewServiceClient(config.StorageAccountUrl, creds, nil)
	if err != nil {
		return nil, err
	}
	return driver, nil
}

func (d *azBlobDriver) obj(bucket, path string) *azblob.BlockBlobClient {
	// Below methods never return an error... why oh why
	bkt, _ := d.client.NewContainerClient(bucket)
	obj, _ := bkt.NewBlockBlobClient(path)
	return obj
}

func (d *azBlobDriver) Get(ctx context.Context, bucket, path string) (file []byte, err error) {
	reader, err := d.GetReader(ctx, bucket, path)
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(reader)
}

func (d *azBlobDriver) GetReader(ctx context.Context, bucket, path string) (reader io.Reader, err error) {
	obj := d.obj(bucket, path)
	resp, err := obj.Download(ctx, nil)
	if err != nil {
		return nil, err
	}
	return resp.Body(nil), nil
}

func (d *azBlobDriver) Set(ctx context.Context, bucket, path string, file []byte) (err error) {
	obj := d.obj(bucket, path)
	_, err = obj.UploadBuffer(ctx, file, azblob.UploadOption{})
	return err
}

func (d *azBlobDriver) SetWithReader(ctx context.Context, bucket, path string, reader io.Reader) (err error) {
	obj := d.obj(bucket, path)
	bytes, err := ioutil.ReadAll(reader)
	if err != nil {
		return err
	}
	_, err = obj.UploadBuffer(ctx, bytes, azblob.UploadOption{})
	return err
}

func (d *azBlobDriver) Del(ctx context.Context, bucket, path string) (err error) {
	obj := d.obj(bucket, path)
	_, err = obj.Delete(ctx, nil)
	return err
}

func (d *azBlobDriver) Close() error {
	return nil
}
