package iwantstorage

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v3"
)

type ErrBadConfig struct {
	Err error
}

func (b *ErrBadConfig) Error() string {
	return fmt.Sprintf("iwanttranslation: bad config: %v", b.Err.Error())
}

func New(ctx context.Context, config *StorageConfig) (client StorageClient, err error) {
	instance := &storageClientInstance{config: config}
	if config.GCS != nil {
		instance.driver, err = newGcsDriver(ctx, config.GCS)
		if err != nil {
			return nil, err
		}
	} else if config.AzBlob != nil {
		instance.driver, err = newAzBlobDriver(ctx, config.AzBlob)
		if err != nil {
			return nil, err
		}
	} else if config.Redis != nil {
		instance.driver, err = newRedisDriver(ctx, config.Redis)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, &ErrBadConfig{errors.New("one of gcs, azBlob, or redis must be set")}
	}
	return instance, nil
}

func NewFromEnv(ctx context.Context) (client StorageClient, err error) {
	config := &StorageConfig{}
	driver := os.Getenv("I_WANT_STORAGE_DRIVER")
	switch driver {
	case "GCS":
		credsPath := os.Getenv("I_WANT_STORAGE_GCS_CREDENTIALS_PATH")
		credentials, err := ioutil.ReadFile(credsPath)
		if err != nil {
			return nil, &ErrBadConfig{fmt.Errorf("bad value for env I_WANT_STORAGE_GCS_CREDENTIALS_PATH %v: %v", credsPath, err)}
		}
		config.GCS = &GCS{
			Credentials: string(credentials),
		}
	case "AzBlob":
		config.AzBlob = &AzBlob{}
		creds := os.Getenv("I_WANT_STORAGE_AZ_BLOB_CREDENTIAL")
		switch creds {
		case "ClientSecret":
			config.AzBlob.ClientSecretCredential = &AzBlobClientSecretCredential{
				TenantId:     os.Getenv("I_WANT_STORAGE_AZ_BLOB_CLIENT_SECRET_TENANT_ID"),
				ClientId:     os.Getenv("I_WANT_STORAGE_AZ_BLOB_CLIENT_SECRET_CLIENT_ID"),
				ClientSecret: os.Getenv("I_WANT_STORAGE_AZ_BLOB_CLIENT_SECRET_CLIENT_SECRET"),
			}
		default:
			return nil, &ErrBadConfig{
				fmt.Errorf("bad value for env I_WANT_STORAGE_AZ_BLOB_CREDENTIAL %v: valid choices are ClientSecret", creds),
			}
		}
	case "Redis":
		config.Redis = &Redis{}
		config.Redis.URL = os.Getenv("I_WANT_STORAGE_REDIS_URL")
		config.Redis.Expiry = os.Getenv("I_WANT_STORAGE_REDIS_EXPIRY")
	default:
		return nil, &ErrBadConfig{fmt.Errorf("bad value for env I_WANT_STORAGE_DRIVER %v: valid choices are GCS, AzBlob", driver)}
	}
	return New(ctx, config)
}

func NewFromConfigFile(ctx context.Context, path string) (StorageClient, error) {
	configFile, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, &ErrBadConfig{fmt.Errorf("file %v couldn't be read: %v", path, err)}
	}
	var config StorageConfig
	err = yaml.Unmarshal(configFile, &config)
	if err != nil {
		return nil, &ErrBadConfig{fmt.Errorf("file %v couldn't be converted from yaml: %v", path, err)}
	}
	return New(ctx, &config)
}

func (s *storageClientInstance) Get(ctx context.Context, bucket, path string) (file []byte, err error) {
	return s.driver.Get(ctx, bucket, path)
}

func (s *storageClientInstance) GetReader(ctx context.Context, bucket, path string) (reader io.Reader, err error) {
	return s.driver.GetReader(ctx, bucket, path)
}

func (s *storageClientInstance) Set(ctx context.Context, bucket, path string, file []byte) (err error) {
	return s.driver.Set(ctx, bucket, path, file)
}

func (s *storageClientInstance) SetWithReader(ctx context.Context, bucket, path string, reader io.Reader) (err error) {
	return s.driver.SetWithReader(ctx, bucket, path, reader)
}

func (s *storageClientInstance) Del(ctx context.Context, bucket, path string) (err error) {
	return s.driver.Del(ctx, bucket, path)
}

func (s *storageClientInstance) Close() error {
	return s.driver.Close()
}

func (s *storageClientInstance) GetConfig() *StorageConfig {
	return s.config
}
