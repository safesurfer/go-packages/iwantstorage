package iwantstorage

import (
	"context"
	"io"
	"time"

	"cloud.google.com/go/storage"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob"
	"github.com/go-redis/redis/v8"
)

type Redis struct {
	// A redis connection URL as documented here:
	// https://pkg.go.dev/github.com/go-redis/redis/v8#ParseURL
	URL string `json:"url"`
	// The expiry time to set on all added elements, or the empty string to not
	// set an expiry.
	// Parsed as a go duration.
	Expiry string `json:"expiry"`
}

type GCS struct {
	// The gcp service account JSON file that will allow
	// access to GCS when used as auth.
	Credentials string `yaml:"credentials"`
}

type AzBlobClientSecretCredential struct {
	TenantId     string `yaml:"tenantId"`
	ClientId     string `yaml:"clientId"`
	ClientSecret string `yaml:"clientSecret"`
}

type AzBlob struct {
	// If not nil, use a client secret to obtain access.
	// TODO: support more auth methods
	ClientSecretCredential *AzBlobClientSecretCredential `yaml:"clientSecretCredential"`
	// The URL to access the storage account, e.g. https://<your_storage_account>.blob.core.windows.net
	StorageAccountUrl string `yaml:"storageAccountUrl"`
}

type StorageConfig struct {
	// If not nil, use Google Cloud Storage as a storage provider.
	GCS *GCS `yaml:"gcs"`
	// If not nil, use Azure blob storage as a storage provider.
	AzBlob *AzBlob `yaml:"azBlob"`
	// If not nil, use Redis as a storage provider.
	Redis *Redis `yaml:"redis"`
	// Option hint for which bucket the client application should use.
	// This is mainly useful for when a client application blindly uses
	// a StorageConfig and wants to know which bucket to use.
	BucketHint string `yaml:"bucketHint"`
}

type gcsDriver struct {
	client *storage.Client
	config *GCS
}

type azBlobDriver struct {
	client *azblob.ServiceClient
	config *AzBlob
}

type redisDriver struct {
	client         redis.UniversalClient
	config         *Redis
	parsedDuration time.Duration
}

type storageClientInstance struct {
	config *StorageConfig
	driver StorageDriver
}

type StorageDriver interface {
	// Get a file with the path in the bucket. Returns ErrFileNotFound
	// if it doesn't exist.
	Get(ctx context.Context, bucket, path string) (file []byte, err error)
	GetReader(ctx context.Context, bucket, path string) (reader io.Reader, err error)
	// Set the contents of a file with the path in the bucket.
	Set(ctx context.Context, bucket, path string, file []byte) (err error)
	SetWithReader(ctx context.Context, bucket, path string, reader io.Reader) (err error)
	// Delete the file with the path in the bucket. It is not an error
	// for the file to not exist.
	Del(ctx context.Context, bucket, path string) (err error)
	// Release the client.
	Close() error
}

type StorageClient interface {
	GetConfig() *StorageConfig
	StorageDriver
}
