package iwantstorage

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"time"

	"github.com/go-redis/redis/v8"
)

func newRedisDriver(ctx context.Context, config *Redis) (driver *redisDriver, err error) {
	driver = &redisDriver{config: config}
	opt, err := redis.ParseURL(config.URL)
	if err != nil {
		return nil, err
	}
	if config.Expiry != "" {
		driver.parsedDuration, err = time.ParseDuration(config.Expiry)
		if err != nil {
			return nil, err
		}
	}
	driver.client = redis.NewClient(opt)
	return driver, nil
}

func (d *redisDriver) key(bucket, path string) string {
	return fmt.Sprintf("%v/%v", bucket, path)
}

func (d *redisDriver) Get(ctx context.Context, bucket, path string) (file []byte, err error) {
	return d.client.Get(ctx, d.key(bucket, path)).Bytes()
}

func (d *redisDriver) GetReader(ctx context.Context, bucket, path string) (reader io.Reader, err error) {
	b, err := d.Get(ctx, bucket, path)
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(b), nil
}

func (d *redisDriver) Set(ctx context.Context, bucket, path string, file []byte) (err error) {
	return d.client.Set(ctx, d.key(bucket, path), file, d.parsedDuration).Err()
}

func (d *redisDriver) SetWithReader(ctx context.Context, bucket, path string, reader io.Reader) (err error) {
	file, err := ioutil.ReadAll(reader)
	if err != nil {
		return err
	}
	return d.Set(ctx, bucket, path, file)
}

func (d *redisDriver) Del(ctx context.Context, bucket, path string) (err error) {
	return d.client.Del(ctx, d.key(bucket, path)).Err()
}

func (d *redisDriver) Close() error {
	return d.client.Close()
}
