package iwantstorage

import (
	"context"
	"io"
	"io/ioutil"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

func newGcsDriver(ctx context.Context, config *GCS) (driver *gcsDriver, err error) {
	driver = &gcsDriver{config: config}
	driver.client, err = storage.NewClient(ctx, option.WithCredentialsJSON([]byte(config.Credentials)))
	if err != nil {
		return nil, err
	}
	return driver, nil
}

func (s *gcsDriver) obj(bucket, path string) *storage.ObjectHandle {
	return s.client.Bucket(bucket).Object(path)
}

func (s *gcsDriver) Get(ctx context.Context, bucket, path string) (file []byte, err error) {
	reader, err := s.GetReader(ctx, bucket, path)
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(reader)
}

func (s *gcsDriver) GetReader(ctx context.Context, bucket, path string) (reader io.Reader, err error) {
	return s.obj(bucket, path).NewReader(ctx)
}

func (s *gcsDriver) Set(ctx context.Context, bucket, path string, file []byte) (err error) {
	w := s.obj(bucket, path).NewWriter(ctx)
	_, err = w.Write(file)
	if err != nil {
		return err
	}
	return w.Close()
}

func (s *gcsDriver) SetWithReader(ctx context.Context, bucket, path string, reader io.Reader) (err error) {
	w := s.obj(bucket, path).NewWriter(ctx)
	_, err = io.Copy(w, reader)
	if err != nil {
		return err
	}
	return w.Close()
}

func (s *gcsDriver) Del(ctx context.Context, bucket, path string) (err error) {
	return s.obj(bucket, path).Delete(ctx)
}

func (s *gcsDriver) Close() error {
	return s.client.Close()
}
